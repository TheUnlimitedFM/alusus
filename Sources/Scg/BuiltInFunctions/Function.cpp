/**
 * @file Scg/BuiltInFunctions/Function.cpp
 *
 * @copyright Copyright (C) 2015 Rafid Khalid Abdullah
 *
 * @license This file is released under Alusus Public License, Version 1.0.
 * For details on usage and copying conditions read the full license in the
 * accompanying license file or at <http://alusus.net/alusus_license_1_0>.
 */
//==============================================================================

#include <prerequisites.h>

// LLVM header files
#include <llvm/IR/IRBuilder.h>

// Scg files
#include <BuiltInFunctions/Function.h>
#include <Containers/Block.h>
#include <Types/DoubleType.h>

namespace Scg
{
}

