/**
 * @file Scg/Operators/CastingOperator.h
 *
 * @copyright Copyright (C) 2014 Rafid Khalid Abdullah
 *
 * @license This file is released under Alusus Public License, Version 1.0.
 * For details on usage and copying conditions read the full license in the
 * accompanying license file or at <http://alusus.net/alusus_license_1_0>.
 */
//==============================================================================

#ifndef __CastingOperator_h__
#define __CastingOperator_h__

namespace Scg
{
// TODO: Mark the class as abstract somehow.
/**
 * Represents a binary operator.
 */
class CastingOperator : public Expression
{
};
}

#endif // __CastingOperator_h__
