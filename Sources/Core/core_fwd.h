/**
 * @file Core/core_fwd.h
 * Contains forward declarations for Alusus Core. The file is still incomplete.
 *
 * @copyright Copyright (C) 2015 Sarmad Khalid Abdullah
 *
 * @license This file is released under Alusus Public License, Version 1.0.
 * For details on usage and copying conditions read the full license in the
 * accompanying license file or at <http://alusus.net/alusus_license_1_0>.
 */
//==============================================================================

namespace Core
{
namespace Processing
{
class BuildMsgStore;
}

namespace Data
{
class SharedMap;
class Module;
class ParsedList;
class ParsedToken;
}

namespace Basic
{
class IdentifiableObject;
}
}
